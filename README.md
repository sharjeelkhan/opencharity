# Open Charity

## Custom Theme for Drupal
This theme is generated for the task given by compucorp. Theme is based on custom theme template without using any library.

## Approach
We can make template in two ways:

###1- Using Blocks.
Blocks can be created by using drupal admin panel. These blocks are pre-defined in .info files and assign their position in *page.tpl.php*. Blocks code can be customized by creating its template file like *block--block-1.tpl.php*, in this case *block-1* is the name of block given in .info file replacing *_* with *-*. This theme's home screen is based on blocks. Mostly home screen are created using this approach in drupal.

###2- Using Content Types.
Content types are just like pages in drupal. You can add fields set their positions in their respective *.tpl.php* file. The process of customizing content types are same as above but we are not going to define it in .info files. We can use blocks in content types for repetitive  content like side bars, footer etc.

## Scripting
I have used some javascript to toggle menu in mobile view. Due to lack of time I can't be able to run sliders, I can make these sliders to make it completely running website.

## Modules
I have used one custom module *Image Blocks* to add images in blocks.

