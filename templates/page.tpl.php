<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to main-menu administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>
<header>
  <div class="container">
    <?php if ($logo): ?>
      <div class="logo">
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>"/></a>
      </div>
    <?php endif; ?>
    <div class="mobileNav">
      <a id="mobileMenu"><i class="fa fa-bars"></i></a>
    </div>
    <nav id="mainMenu" class="mainMenu" data-action="hide">
      <ul>
      <?php
        $mainMenu = menu_navigation_links('main-menu');
        $itemCount = 0;
        foreach( $mainMenu as $menuItem){
          $className = str_replace("#", "", $menuItem['attributes']['title']).'Link';
      ?>
      <li <?php if($itemCount == 0){?>class="active"<?php }?>><a class="scroll <?php print $className;?>" data-link="<?php print $className;?>" href="<?php print $menuItem['attributes']['title']?>"><?php print $menuItem['title']?></a></li>
      <?php
        $itemCount++;
        }
      ?>
      </ul>
    </nav>
    <div class="clearFix"></div>
  </div>
</header>

<?php if ($is_front): ?>
  <?php print render($page['home_banner_text']); ?>
  <section id="register" class="eventStrip">
    <div class="container">
      <div class="row">
        <?php print render($page['home_event_text']); ?>
        <div class="col-2 col-sm-1">
          <a href="#">Register</a>
        </div>
        <div class="clearFix"></div>
      </div>
    </div>
  </section>
  <section class="getInvolved">
    <div class="container">
      <div class="row">
        <?php print render($page['home_involved_heading']); ?>
        <?php print render($page['home_involved_block']); ?>
        <div class="clearFix"></div>
      </div>
    </div>
  </section>
  <section id="ourMission" class="ourMission">
    <div class="container">
      <div class="row">
        <div>
          <?php print render($page['home_mission_heading_text']); ?>
        </div>
          <?php print render($page['home_mission_block']); ?>
        <div class="clearFix"></div>
        <hr/>
      </div>
    </div>
  </section>
  <section class="ourMember">
    <div class="container">
      <div class="row">
        <?php print render($page['home_member_heading_text']); ?>
        <ul class="scroller">
        <?php print render($page['home_members_block']); ?>
        </ul>
        <div class="clearFix"></div>
        <ul class="scrollPagination">
          <li class="selected">
            <a></a>
          </li>
          <li>
            <a></a>
          </li>
          <li>
            <a></a>
          </li>
        </ul>
      </div>
    </div>
  </section>
  <section id="blog" class="blog">
    <div class="container">
      <div class="row">
        <?php print render($page['home_blog_heading_text']); ?>
        <ul>
          <?php print render($page['home_blog_block']); ?>
        </ul>
        <div class="clearFix"></div>
        <div class="blogNav">
          <a href="#" class="prev"><i class="fa fa-angle-left"></i></a>
          <a href="#" class="next"><i class="fa fa-angle-right"></i></a>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>

<!-- Footer -->
<footer>
  <div class="container">
    <ul class="social">
      <?php print render($page['footer_social']); ?>
    </ul>
    <hr/>
    <?php print render($page['footer_copyright']); ?>
  </div>
</footer>
<script type="text/javascript">
var mobileMenu = document.querySelector('#mobileMenu');
var mainMenu = document.querySelector('#mainMenu');
mobileMenu.addEventListener('click', function (event) {
    if (mainMenu.dataset.action == "hide") {
        mainMenu.setAttribute('class','mainMenu showMenu');
        mainMenu.dataset.action = 'show';
    } else {
        mainMenu.setAttribute('class','mainMenu');
        mainMenu.dataset.action = 'hide';
    }
  }
);
</script>